#dcl-land-manager

DCL Land Manager

Why?
1. When creating a multi-parcel scene, you can make mistakes when trying to create the parcels list in scene.json.  This tool automates the generation of that list, to be copied and pasted into your scene.json file
2. When assigning a tenants wallet to be the LandUpdateOperator of all the parcels in a scene within an estate (using the setLandManyLandUpdateOperator method on the EstateProx contract as called by the estate's UpdateManager), you need a list of all the LandIDs for one of the parameters.  This tool automatically looks up and generates a list of the LandIDs for all the parcels in the scene.

Installation:
1. git clone https://gitlab.com/dcl-tech/dcl-land-manager
2. npm i
3. npm run build
4. npm start
5. // open browser at http://localhost:8080
6. // confirm/test
7. // promote the contents of the dist dir to a website location
8. // confirm/test there

Use:

Generate Coordinates and put them into scene.json
1. Enter the W, E, S and N parcel coordinate limits for a rectangular scene.
2. Press Generate Coordinates.
3. Copy the resulting parcel coordinates list into scene.json, replacing the value of "parcels"  that is there.  You can discard some coordinates if your scene isn't rectangular.   Be sure to put the first coordinate (or some coordinate from the list) into the value for "base".

Generate LandIDs and use them in the setManyLandUpdateOperator function
1. Log into MetaMask.  The account you use doesn't matter, although you might find it efficient to use the estate's UpdateManager account.
2. Enter the W, E, S and N parcel coordinate limits for the rectangular scene.
3. Press Generate LandIDs.  Copy the list to a safe place.  Delete rows if your scene isnt rectangular.
4. Use a tool that can execute a contract method call through a proxy contract.  An easy choice is https://abitopic.io/0x959e104e1a4db6317fa58f8295f586e1a978c297?network=mainnet&isProxy=1.
5. Obtain the operator's wallet that will be assigned.
6. Obtain the estate ID, e.g. from https://market.decentraland.org or from the District Land Management Guide at https://docs.google.com/document/d/1lsn8yDgGRZFuoZyw8raL4rVPr1cHbAp5BQeChA1lbIo/edit#heading=h.gcsb7a43nghw.
4. Set up the parameters for the call to setManyLandUpdateOperator in the contract tool from step 4.
5. For the LandIds field, copy and paste the list from step 3 into the field.
6. Execute the call, signing it with the estate's UpdateManager wallet.  Use 7000000 gas (excess will be returned to your wallet) and bump up the gas price by 2 (so the transaction will execute in under a minute).  Typically the transaction costs less then $1 USD.


